# Obsidian Publish Self-Hosted Template

[Demo](https://krenyy.gitlab.io/opsh-template)

Big thanks to the [Obsidian](https://obsidian.md/) team for [Obsidian Publish](https://obsidian.md/publish). If you like their work, be sure to support them!
