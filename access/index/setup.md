All paths are relative to the root of the [repository][repo].

Your notes go in `access/${VAULT_NAME}/`.

For each vault, there can be a `options/${VAULT_NAME}.json` file for storing [[options]]. If it doesn't exist, it will use default values.

To add your custom [[styles]], use the [official workaround][obsidian-style-workaround].

Once you have at least your notes in the correct directory, you can proceed.

## GitLab Pages

The `.gitlab-ci.yml` I made should do all heavy-lifting for you. Push your changes and you're done!

## Manual

Figure it out by reading `.gitlab-ci.yml`.

[gitlab-pages]: https://pages.gitlab.io/
[repo]: https://gitlab.com/krenyy/opsh-builder
[obsidian-style-workaround]: https://help.obsidian.md/How+to/Add+custom+styles#Obsidian+Publish+styles

[^spa]: Single-page application
