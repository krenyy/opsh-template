I loved how [Obsidian Publish] looked and worked, but hated the fact that it's paid and cannot be self-hosted.

That's why I made [this repo] to make it really easy to host your own independent instance.

This demo instance uses the [Clair de Lune] CSS theme.

I will take you through the steps to publish your own [Zettelkasten] for free[^free] [here](setup).

You can even host [multiple vaults] simultaneously!

[obsidian publish]: https://publish.obsidian.md
[this repo]: https://gitlab.com/krenyy/opsh-builder
[clair de lune]: https://github.com/jamiebrynes7/clair-de-lune-obsidian-theme
[zettelkasten]: https://en.wikipedia.org/wiki/Zettelkasten
[multiple vaults]: ./secret

[^free]: As opposed to paying either $192/year or $20/month per site
